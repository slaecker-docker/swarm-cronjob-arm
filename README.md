Cronjobs for Docker Swarm (swarm-cronjobs) adapted to ARM.

Original Project Repo:
https://github.com/crazy-max/swarm-cronjob

Built on GitLab:
* Commits: https://gitlab.com/slaecker-docker/swarm-cronjob-arm/commits/master
* Jobs: https://gitlab.com/slaecker-docker/swarm-cronjob-arm/-/jobs